use std::{io::{self, Write, BufRead}, fs, marker::PhantomData, str::FromStr};

use ed25519_dalek::{Keypair, Signer, SecretKey, PublicKey, SECRET_KEY_LENGTH, PUBLIC_KEY_LENGTH, KEYPAIR_LENGTH, SIGNATURE_LENGTH, Verifier, Signature};
use rand::rngs::OsRng;

const PUBLIC: &[u8; PUBLIC_KEY_LENGTH] = include_bytes!("../public.key");
// const KEYPAIR: &[u8; KEYPAIR_LENGTH] = include_bytes!("../authority.key");

#[derive(Debug)]
enum LicenseType {
    Standard,
    Enterprise,
    Professional,
}

impl From<&str> for LicenseType {
    fn from(license_type: &str) -> Self {
        match license_type {
            "Standard" => Self::Standard,
            "Enterprise" => Self::Enterprise,
            "Professional" => Self::Professional,
            _ => unreachable!()
        }
    }
}

impl AsRef<str> for LicenseType {
    fn as_ref(&self) -> &'static str {
        match *self {
            LicenseType::Standard => "Standard",
            LicenseType::Enterprise => "Enterprise",
            LicenseType::Professional => "Professional",
        }
    }
}

#[derive(Debug)]
struct Privileged;

#[derive(Debug)]
struct Unprivileged;

#[derive(Debug)]
struct License<A> {
    license_string: String,
    signature: [u8; SIGNATURE_LENGTH],
    authority: PhantomData<A>
}

impl License<Privileged> {
    pub fn authorize_from_parts<S1: AsRef<str>, S2: AsRef<str>>(license_holder: S1, license_type: S2, authority: &Keypair) -> Self {
        let license_string = format!("{}@{}", license_holder.as_ref(), license_type.as_ref());
        let signature = authority.sign(license_string.as_bytes()).to_bytes();
        Self {
            license_string,
            signature,
            authority: PhantomData
        }
    }

    pub fn dump_to_file(&self) -> io::Result<()> {
        let mut file_handle = fs::File::options().create(true).write(true).open("./license.dat")?;
        file_handle.write(b"---------------- LICENSE BEGIN ----------------\n")?;
        file_handle.write(self.license_string.as_bytes())?;
        file_handle.write(b"\n")?;

        let encoded = hex::encode_upper(self.signature);
        let encoded = encoded
            .chars()
            .enumerate()
            .fold(String::new(), |mut buffer, (i, c)| {
                const CHUNKS: usize = 4;
                const CHUNKS_PER_LINE: usize = 8;
                buffer.push(c);
                // Format nicely; bytes are grouped into CHUNKS separated by spaces, with a newline every CHUNKS_PER_LINE
                if i % (CHUNKS * CHUNKS_PER_LINE) == (CHUNKS * CHUNKS_PER_LINE) - 1 && i != encoded.len() - 1 {
                    buffer.push('\n');
                } else if i % CHUNKS == CHUNKS - 1 && i != encoded.len() - 1 {
                    buffer.push(' ');
                }
                buffer
            });
        file_handle.write(encoded.as_bytes())?;
        file_handle.write(b"\n---------------- LICENSE END   ----------------")?;

        Ok(())
    }
}

impl License<Unprivileged> {
    pub fn validate_from_parts<S1: AsRef<str>, S2: AsRef<str>>(license_holder: S1, license_type: S2, signature_b: [u8; SIGNATURE_LENGTH], authority: &PublicKey) -> Option<Self> {
        let license_string = format!("{}@{}", license_holder.as_ref(), license_type.as_ref());

        let signature = Signature::from_bytes(&signature_b).ok()?;
        authority.verify(license_string.as_bytes(), &signature).ok()?;

        Some(Self {
            license_string,
            signature: signature_b,
            authority: PhantomData
        })
    }

    pub fn validate_from_file(authority: &PublicKey) -> Option<Self> {
        let file_handle = fs::File::options().read(true).open("license.dat").ok()?;
        let mut lines = io::BufReader::new(file_handle).lines();

        // Check LICENSE BEGIN header
        match lines.next().as_ref() {
            Some(Ok(s)) if s == "---------------- LICENSE BEGIN ----------------" => {},
            _ => return None,
        };

        // Extract license info, (licensee, license type)
        let license_string = lines.next()?.ok()?;
        // Load and decode the HEX-ENCODED signature of the license info
        let signature_b = {
            let mut signature = lines.by_ref().take(4).collect::<Result<String, _>>().ok()?;
            signature.retain(|c| c != ' ');
            let mut buffer = [0; SIGNATURE_LENGTH];
            hex::decode_to_slice(signature, &mut buffer).ok()?;
            buffer
        };

        // Check LICENSE END header
        match lines.next().as_ref() {
            Some(Ok(s)) if s == "---------------- LICENSE END   ----------------" => {},
            _ => return None,
        };

        // Simple authenticity check, EASY TO BYPASS, FIXME
        let signature = Signature::from_bytes(&signature_b).ok()?;
        authority.verify(license_string.as_bytes(), &signature).ok()?;

        Some(Self {
            license_string,
            signature: signature_b,
            authority: PhantomData
        })
    }
}


impl<T> License<T> {
    #[inline]
    pub fn get_licensee(&self) -> &str {
        &self.license_string.split("@").next().unwrap()
    }

    #[inline]
    pub fn get_license_type(&self) -> LicenseType {
        let license_type = self.license_string.split("@").skip(1).next().unwrap();
        LicenseType::from(license_type)
    }
}

fn main() {
    {
        // let keypair = Keypair::from_bytes(KEYPAIR).unwrap();
        // let license = License::authorize_from_parts("hippoz", LicenseType::Standard, &keypair);
        // license.dump_to_file().unwrap();
        // println!("{:?}", license);
    }


    {
        let authority = PublicKey::from_bytes(PUBLIC).unwrap();
        match License::validate_from_file(&authority) {
            Some(license) => {
                println!("Licensed edition \"{}\" to \"{}\"", license.get_license_type().as_ref(), license.get_licensee());
            },
            None => println!("License invalid"),
        }
    }
}

fn generate_keys() -> io::Result<()> {
    let mut csprng = OsRng{};
    let keypair: Keypair = Keypair::generate(&mut csprng);
    let keypair_public = keypair.public.to_bytes();
    let keypair_private = keypair.to_bytes();
    fs::write("public.key", keypair_public)?;
    fs::write("authority.key", keypair_private)
}